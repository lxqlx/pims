// parse request params
var q1 = $.url().param('q1');
$('input[name="q1"]').val(q1);
var keywords = [];
if (/\S/.test(q1)) {//if not only white spaces
	if (q1) {
		keywords = String(q1).trim()
		keywords = keywords.replace(/  +/g, ' ');
		keywords = keywords.split(" ");
	}
}
else {
	keywords = [q1];
}

var emps = [];
var currentView = getCurrentView();//"showGeneralView", "showLargeView", "showDetailedView"
displayConvertTable();
// read data from database
if (q1) {
	//emps = dbQuery.queryMultiKeywords(q1);
	dbQuery.queryMultiKeywordsWithMatchedWords(keywords);
}else {
	emps = alasql('SELECT * FROM emp', []);
}
//$('#pagination-view').twbsPagination({
//    totalPages: 35,
//    visiblePages: 7,
//    onPageClick: function (event, page) {
//        $('#page-content').text('Page ' + page);
//    }
//});
// create employee list
empViews[currentView]();
dbQuery.showMatchedItems();
//switch View
$(document).on('change', 'input:radio', function (event) {
	if ($('#showLargeView').prop('checked')) {
		setCurrentView("showLargeView");
		empViews.showLargeView();
	}
	if ($('#showGeneralView').prop('checked')) {
		setCurrentView("showGeneralView");
		empViews.showGeneralView();
	}
	if ($('#showDetailedView').prop('checked')) {
		setCurrentView("showDetailedView");
		empViews.showDetailedView();
	}
});
//after being fixed at top, the width will change. Just to correct the width;
var $affixElement = $('div[data-spy="affix"]');
$affixElement.width($('div#general-view').width());
$(window).resize(function(){
	var $affixElement = $('div[data-spy="affix"]');
	$affixElement.width($('#general-view').width());
});

$("a#table-customization").click(function(){
	try {
		localStorage.setItem("dbQuery", JSON.stringify(dbQuery));
		if (currentView == 'showGeneralView')
			localStorage.setItem("ExisitingTable", $('div#general-view').html());
		else if (currentView == 'showDetailedView')
			localStorage.setItem("ExisitingTable", $('div#detailed-view').html());
	}
	catch (e) {
		console.log("Storage currentView failed: " + e);
		alert("Data Error!");
	}
})

//-----------------general functions--------------------------------
//disable convert to table when large view.
function displayConvertTable() {
		$('a#table-customization').show();
};
//remove duplicate employees
function employeeArrayUnique(odlArray) {
	var newArray = odlArray.concat();
	for(var i=0; i<newArray.length; ++i) {
		for(var j=i+1; j<newArray.length; ++j) {
			if(newArray[i].id == newArray[j].id)
				newArray.splice(j--, 1);
		}
	}
	return newArray;
};
//Employees intersection
function employeeArrayIntersection(a, b) {
	var ai=0, bi=0;
	var result = [];

	//console.log("a : ");
	//console.log(a);
	//console.log("b : ");
	//console.log(b);
	while( ai < a.length && bi < b.length )
	{
		if (a[ai].id < b[bi].id ){ ai++; }
		else if (a[ai].id > b[bi].id ){ bi++; }
		else /* they're equal */
		{
			result.push(a[ai]);
			ai++;
			bi++;
		}
	}
	return result;
}
function generateMatchedItemsTable(id, showMap) {
	var matched = dbQuery.empFullInfo[id].matched;
	var countRowSpan = function(list, check){
		var count = 0;
		for (var key in list) {
			if (check[key])
				count++;
		}
		return count;
	};
	var td = $("<td id='matchedItems'></td>");
	var matchedTable = $('<table class="table table-bordered" ></table>');
	for (var keyword in matched) {
		var tr = $("<tr></tr>");
		var items = matched[keyword];
		tr.append("<th rowspan=" + countRowSpan(items, showMap[keyword]) + ">" + keyword + "</th>");
		for (item in items) {
			if (!showMap[keyword][item]) {
				//console.log("table exclude keyword: " + keyword + ", item : " + item);
				continue;//skip excluded item
			}

			var value = items[item];
			if ($.inArray(item, dbQuery.itemsNeedToBeConverted) != -1) {
				value = DB.choice(value);
			}
			var intableExclude = $("<td class='intable-exclude' id='" + keyword+"-"+item + "'>" + item + " <button type='button' class='close' data-dismiss='alert' aria-label='Close' data-toggle='tooltip' data-placement='top' title='Exclude this item'><span aria-hidden='true'>×</span></button></td>");
			intableExclude.appendTo(tr);
			intableExclude.find('button').on('click', function(){
				var ki = $(this).parent().attr('id').split("-");
				var keyword = ki[0];
				var item = ki[1];
				//console.log("intable-exclude click " + keyword + ", " + item);
				$('tr#'+keyword).find(":checkbox[id='"+item+"']").click();
			});
			tr.append("<td class='success'>" + empViews.highlightKeyWord([[keyword], value]) + "</td>");
			tr.appendTo(matchedTable);
			tr = $("<tr></tr>");
		}
	}
	matchedTable.appendTo(td);
	return td;
};
function escapeSearchKeyword(keyword){
	return keyword.replace(/'/g, "''").replace(/([!&#$%_^])/g, "!$1").replace(/\[/g, "[[]").replace(/\\/g, "!\\\\");
};
function escapeHighlightKeyword(keyword){
	return keyword.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
};
function getCurrentView() {
	var storageFiles = JSON.parse(localStorage.getItem("currentView")) || {};

	if (jQuery.isEmptyObject(storageFiles)) {
		storageFiles.view = "showGeneralView";
		try {
			localStorage.setItem("currentView", JSON.stringify(storageFiles));
		}
		catch (e) {
			console.log("Storage currentView failed: " + e);
		}
	}
	//console.log(storageFiles);
	$('#'+storageFiles.view).click();
	return storageFiles.view;
};
function setCurrentView(view) {
	currentView = view;
	displayConvertTable();
	var storageFiles = JSON.parse(localStorage.getItem("currentView")) || {};

	storageFiles.view = view;
	try {
		localStorage.setItem("currentView", JSON.stringify(storageFiles));
	}
	catch (e) {
		console.log("Storage currentView failed: " + e);
	}
};