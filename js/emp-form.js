// options: gender
var sexes = DB.choices('sex');
for (var i = 0; i < sexes.length; i++) {
	var sex = sexes[i];
	$('<option>').attr('value', sex.id).text(sex.text).appendTo($('#sex select'));
}

// options: type of lending
var rentals = DB.choices('rental');
for (var i = 0; i < rentals.length; i++) {
	var rental = rentals[i];
	$('<option>').attr('value', rental.id).text(rental.text).appendTo($('#rental select'));
}

var id = parseInt($.url().param('id'));
if (id) {
	// read data from database
	var emp = alasql('SELECT * FROM emp WHERE id=?', [ id ])[0];
	$('#number input').val(emp.number);
	$('#name input').val(emp.name);
	$('#sex select').val(emp.sex);
	$('#birthday input').val(emp.birthday);
	$('#tel input').val(emp.tel);
	$('#ctct_name input').val(emp.ctct_name);
	$('#ctct_addr input').val(emp.ctct_addr);
	$('#ctct_tel input').val(emp.ctct_tel);
	$('#pspt_no input').val(emp.pspt_no);
	$('#pspt_date input').val(emp.pspt_date);
	$('#pspt_name input').val(emp.pspt_name);
	$('#rental select').val(emp.rental);

	// update image and name
	$('#img-emp').attr('src', 'img/' + emp.id + '.jpg');
	$('#div-name').text(emp.name);
	$('#div-number').text(emp.number);
	$('#nav-name').text(emp.name);
	$('#nav-emp').attr('href', 'emp.html?id=' + id).text(emp.name);
}

// save
function update() {
	var emp = [];
	emp.push($('#number input').val());
	emp.push($('#name input').val());
	emp.push(parseInt($('#sex select').val()));
	emp.push($('#birthday input').val());
	emp.push($('#tel input').val());
	emp.push($('#ctct_name input').val());
	emp.push($('#ctct_addr input').val());
	emp.push($('#ctct_tel input').val());
	emp.push($('#pspt_no input').val());
	emp.push($('#pspt_date input').val());
	emp.push($('#pspt_name input').val());
	emp.push(parseInt($('#rental select').val()));

	if (id) {
		emp.push(id);
		alasql(
				'UPDATE emp SET \
				number = ?, \
				name = ?, \
				sex = ?, \
				birthday = ?, \
				tel = ?, \
				ctct_name = ?, \
				ctct_addr = ?, \
				ctct_tel = ?, \
				pspt_no = ?, \
				pspt_date = ?, \
				pspt_name = ?, \
				rental = ? \
				WHERE id = ?',
				emp);
		uploadImgToLocal(id);
	} else {
		id = alasql('SELECT MAX(id) + 1 as id FROM emp')[0].id;
		emp.unshift(id);
		alasql(
				'INSERT INTO emp(\
				id, \
				number, \
				name, \
				sex, \
				birthday, \
				tel, \
				ctct_name, \
				ctct_addr, \
				ctct_tel, \
				pspt_no, \
				pspt_date, \
				pspt_name, \
				rental) \
				VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?);',
				emp);
		uploadImgToLocal(id);
	}
	window.location.assign('emp.html?id=' + id);
}

//load image
$("img#img-emp").click(function() {
	$("input[id='preview_img']").click();
});
showLocalImg();


function loadImg(event) {
	var output = document.getElementById('img-emp');
	output.src = URL.createObjectURL(event.target.files[0]);
};
// localStorage with image

function uploadImgToLocal(empId) {
	//--------------------------------------->
	console.log("processLocal Image ...");

	var imgId = "img/"+empId+".jpg";
	var storageFiles = JSON.parse(localStorage.getItem("storageFiles")) || {};

	var ele = document.getElementById("img-emp");

	var localImgSrc = ele.getAttribute("src");
	console.log("local image src is " + localImgSrc);

	if (jQuery.isEmptyObject(storageFiles)) {
		storageFiles.images = {};
	}

	//draw local image to base64 data
	//if (localImgSrc != "") {
	var imgW = 150, imgH = 200;

	var id = imgId;
	var img = new Image();
	img.src = localImgSrc;

	var imgCanvas = document.createElement("canvas");
	var imgContext = imgCanvas.getContext("2d");

	// Make sure canvas is as big as the picture
	imgCanvas.width = imgW;
	imgCanvas.height = imgH;
	if (localImgSrc != "") {
		// Draw image into canvas element
		imgContext.drawImage(img, 0, 0, imgW, imgH);
		imgContext.font = "15px Calibri";
		imgContext.fillText("New Photo EMP ID " + empId, 0, imgH/2);
	}
	else {
		imgContext.fillStyle = "white";
		imgContext.fillRect(0, 0, imgW, imgH);
		imgContext.fillStyle = "black";
		imgContext.font = "15px Calibri";
		imgContext.fillText("Please Upload Photo" + empId, 0, imgH/2);
	}
	// Save image as a data URL
	storageFiles.images[id] = imgCanvas.toDataURL("image/jpeg", 1);
	console.log("for id : " + id + ", base 64 data is " + imgCanvas.toDataURL("image/jpeg", 1));
	// Save as JSON in localStorage
	try {
		localStorage.setItem("storageFiles", JSON.stringify(storageFiles));
		console.log("Storage success: " + id + ", elements length " + elements.length);
	}
	catch (e) {
		console.log("Storage failed: " + e);
	}
	//}
};
function showLocalImg() {
	var storageFiles = JSON.parse(localStorage.getItem("storageFiles")) || {};

	var ele = document.getElementById("img-emp");

	var imgId = ele.getAttribute("src");

	if (!jQuery.isEmptyObject(storageFiles)) {
		if (imgId in storageFiles.images)
			ele.setAttribute("src", storageFiles.images[imgId]);
	}
};