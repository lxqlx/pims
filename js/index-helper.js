var dbQuery = {query : 'SELECT emp.* FROM emp WHERE true=false'};
dbQuery.itemsNeedToBeConverted = ["sex", "rental", "emp.sex" ,"emp.rental", "addr.house", "family.sex", "family.cohabit", "family.care"];
dbQuery.colName = {
		'number'    : 'Number',
		'name'      : 'Name',
		'sex'       : 'Gender',
		'birthday'  : 'Date of Birth',
		'tel'       : 'Telephone',
		'ctct_name' : 'Emergency Contact Name',
		'ctct_addr' : 'Emergency Contact Address',
		'ctct_tel'  : 'Emergency Contact Tel',
		'pspt_no'   : 'Passport No.',
		'pspt_date' : 'Passport Date',
		'pspt_name' : 'Passport Name',
		'rental'    : 'Type of Lending',
		'addr.zip'      : 'Zip',
		'addr.state'    : 'State',
		'addr.city'     : 'City',
		'addr.street'   : 'Street',
		'addr.bldg'     : 'Building',
		'addr.house'    : 'Type of Address',
		'edu.school'        : 'School',
		'edu.major'         : 'Dep/Div/Major',
		'edu.grad'          : 'Graduation',
		'family.name'           : 'Name',
		'family.sex'            : 'Gender',
		'family.birthday'       : 'Date of Birth',
		'family.relation'       : 'Relationship',
		'family.cohabit'        : 'Type of Cohabitation',
		'family.care'           : 'Type of Dependent'
};
dbQuery.empCols = ['number', 'name', 'sex', 'birthday', 'tel',
					'ctct_name','ctct_addr', 'ctct_tel', 'pspt_no',
					'pspt_date', 'pspt_name', 'rental',];
dbQuery.empColsPre = ['emp.number', 'emp.name', 'emp.sex', 'emp.birthday', 'emp.tel',
					'emp.ctct_name','emp.ctct_addr', 'emp.ctct_tel', 'emp.pspt_no',
					'emp.pspt_date', 'emp.pspt_name', 'emp.rental',];
dbQuery.addrCols = ['addr.zip', 'addr.state', 'addr.city', 'addr.street', 'addr.bldg', 'addr.house'];
dbQuery.eduCols = ['edu.school', 'edu.major', 'edu.grad'];
dbQuery.familyCols = ['family.name', 'family.sex', 'family.birthday', 'family.relation', 'family.cohabit', 'family.care'];
dbQuery.matchedItems = {};

//results for query single columns
dbQuery.empFullInfo = {};
//When no keyword entered, for detailed views
dbQuery.getAllEmps = function() {
	dbQuery.empFullInfo = {};
	var empResults = alasql('SELECT * FROM emp', []);
	empResults.forEach(function(emp) {
		dbQuery.empFullInfo[emp.id] = {};
		dbQuery.empFullInfo[emp.id].emp = emp;
		dbQuery.empFullInfo[emp.id].matched = {};
	});
	dbQuery.fillAllInfoForResultList();
}
dbQuery.searchAll = function(keyword) {
	console.log("start query...");
	dbQuery.empColsPre.forEach(function(col) {
		dbQuery.queryBySingleColumn(col, keyword);
	});
	console.log("emp finished...");
	dbQuery.addrCols.forEach(function(col) {
		dbQuery.queryBySingleColumn(col, keyword);
	});
	console.log("addr finished...");
	dbQuery.eduCols.forEach(function(col) {
		dbQuery.queryBySingleColumn(col, keyword);
	});
	console.log("edu finished...");
	dbQuery.familyCols.forEach(function(col) {
		dbQuery.queryBySingleColumn(col, keyword);
	});
	console.log("family finished...");
	dbQuery.fillAllInfoForResultList();
};
dbQuery.fillAllInfoForResultList = function() {
	for(var id in dbQuery.empFullInfo) {
		dbQuery.empFullInfo[id].edu = alasql('SELECT * FROM edu WHERE emp=?', [ parseInt(id) ]);
		dbQuery.empFullInfo[id].family = alasql("SELECT * FROM family WHERE emp=?", [ parseInt(id) ]);
		dbQuery.empFullInfo[id].addr = alasql("SELECT * FROM addr WHERE emp=?", [ parseInt(id) ]);
	}
}
dbQuery.queryBySingleColumn = function(col, keyword) {
	var table = col.split('.')[0];
	var colWithoutPrefix = col.split('.')[1];
	var newColName = table+"_"+colWithoutPrefix;
	var value;
	var empResults;
	//console.log(col + " keyword: " + keyword + ", col : " + colWithoutPrefix);
	switch (table) {
		case "family":{
			var sqlStr = "SELECT emp.*, " + col + " AS " + newColName + " FROM emp LEFT JOIN family ON emp.id = family.emp WHERE true=false";
			sqlStr = dbQuery.addCol(col, keyword, sqlStr);
			//console.log(sqlStr);

			empResults = alasql(sqlStr);
			break;
		}
		case "edu":{
			var sqlStr = "SELECT emp.*, " + col + " AS " + newColName + " FROM emp LEFT JOIN edu ON emp.id = edu.emp WHERE true=false";
			sqlStr = dbQuery.addCol(col, keyword, sqlStr);
			//console.log(sqlStr);

			empResults = alasql(sqlStr);
			break;
		}
		case "addr": {
			var sqlStr = "SELECT emp.*, " + col + " AS " + newColName + " FROM emp LEFT JOIN addr ON emp.id = addr.emp WHERE true=false";
			sqlStr = dbQuery.addCol(col, keyword, sqlStr);
			//console.log(sqlStr);

			empResults = alasql(sqlStr);
			break;
		}
		default : {
			var sqlStr = "SELECT emp.* FROM emp WHERE true=false";
			newColName = colWithoutPrefix;//correct new column name
			sqlStr = dbQuery.addCol(col, keyword, sqlStr);
			//console.log(sqlStr);

			empResults = alasql(sqlStr);
		}
	}
	//console.log(empResults);
	for(var i = 0; i < empResults.length; i++) {
		var empR = empResults[i];

		value = empR[newColName];

		if (!jQuery.isEmptyObject(empR) && typeof value !== 'undefined'){
			dbQuery.addMatchedItem(col, keyword);
			if (empR.id in dbQuery.empFullInfo) {
				if (!(keyword in dbQuery.empFullInfo[empR.id].matched)) {
					dbQuery.empFullInfo[empR.id].matched[keyword] = {};
				}

				dbQuery.empFullInfo[empR.id].matched[keyword][col] = value;
				dbQuery.empFullInfo[empR.id].emp = empR;
			}
			else {
				dbQuery.empFullInfo[empR.id] = {};
				dbQuery.empFullInfo[empR.id].matched = {};
				dbQuery.empFullInfo[empR.id].matched[keyword] = {};
				dbQuery.empFullInfo[empR.id].matched[keyword][col] = value;
				dbQuery.empFullInfo[empR.id].emp = empR;
			}
		}

	}

	//console.log(emps);
	//console.log(dbQuery.empFullInfo);
};
dbQuery.addCol = function(col, keyword, input){
	keyword = escapeSearchKeyword(keyword);//escape with !
	if ($.inArray(col, dbQuery.itemsNeedToBeConverted) != -1) {
		var ids = DB.choiceIds(keyword);
		ids = ids.map(function(obj){
			return obj.id;
		});
		if (input == "")
			this.query += " OR (" + col + " IN (" + ids +"))";
		else
			input += " OR (" + col + " IN (" + ids +"))";
	}
	else {
		if (input == "")
			this.query += " OR (" + col + "::String LIKE '%" + keyword +"%')";
		else
			input += " OR (" + col + "::String LIKE '%" + keyword +"%' ESCAPE '!')";
	}
	return input;
};

dbQuery.queryMultiKeywordsWithMatchedWords = function(keywords) {
	//console.log("keywords are: ");
	//console.log(keywords);

	keywords.forEach(function(ele, index) {
		//console.log("element : " + ele);
		dbQuery.searchAll(ele);
	});
};

//Initialize all Matched Items (true include, false exclude in list);
dbQuery.addMatchedItem = function(item, keyword) {
	if (!(keyword in dbQuery.matchedItems)){
		dbQuery.matchedItems[keyword] = {};
	}
	//console.log(item);
	//console.log(keyword)
	//console.log(dbQuery.matchedItems);
	dbQuery.matchedItems[keyword][item] = true;
};
dbQuery.showMatchedItems = function() {
	var div = $("div#matchedItems");
	var table = $("<table class='table borderless'></table");

	for (var keyword in dbQuery.matchedItems) {
		var p = $('<tr id="'+keyword+'"></tr>');
		var checkbox;
		p.append("<th>" + keyword + ": <th>");
		for (var item in dbQuery.matchedItems[keyword]) {
			var td = $('<td></td>');
			var label = $('<label></label>');
			checkbox = $('<input type="checkbox" id="' + item + '" checked><span class="label label-info">' + item + '</span>');
			checkbox.appendTo(label);
			label.appendTo(td);
			td.appendTo(p);
		}
		p.appendTo(table);
		table.appendTo(div);
	}

	$(':checkbox').click(function(){
		var k = $(this).closest('tr').attr('id');
		var i = $(this).attr('id');
		if ($(this).prop('checked')) {
			$(this).next('span').removeClass("label-default").addClass("label-info");
			dbQuery.includeItem(k, i);
		}
		else {
			$(this).next('span').removeClass("label-info").addClass("label-default");
			dbQuery.excludeItem(k, i);
		}
	});
};

dbQuery.labelMatchedItems = function() {

};
dbQuery.excludeItem = function(keyword, item){
	//console.log("excluding keyword: " + keyword + ", item: " + item);
	dbQuery.matchedItems[keyword][item] = false;
	dbQuery.reloadList();
};
dbQuery.includeItem = function(keyword, item){
	//console.log("including keyword: " + keyword + ", item: " + item);
	dbQuery.matchedItems[keyword][item] = true;
	dbQuery.reloadList();
};
dbQuery.reloadList = function() {
	empViews[currentView]();
};
//---------------------empViews Object-------------------------------
var empViews = {};
empViews.loadLocalImage = function() {
	var storageFiles = JSON.parse(localStorage.getItem("storageFiles")) || {};

	var elements = document.getElementsByTagName("img");

	if (jQuery.isEmptyObject(storageFiles)) {
		storageFiles.images = {};
	}
	//console.log(storageFiles.images);

	for (var i = 0; i < elements.length; i++) {
		var ele = elements[i];
		var imgId = ele.getAttribute('src');
		if (!(imgId in storageFiles.images)) {
			ele.setAttribute("src", imgId);
		}
		else {
			ele.setAttribute("src", storageFiles.images[imgId]);
		}
	}
};
empViews.showLargeView = function() {
	if (!q1 && jQuery.isEmptyObject(dbQuery.empFullInfo)) dbQuery.getAllEmps();
	var largeView = $('div#large-view');
	$('div#general-view').find('table').remove();
	$('div#detailed-view').find('table').remove();
	largeView.find('div.employee').remove();
	
	largeView.append("<hr>");

	for (var id in dbQuery.empFullInfo){
		var emp = dbQuery.empFullInfo[id].emp;
		var show = true;
		for (var i = 0; i < keywords.length; i++) {
			if (keywords[i] in dbQuery.empFullInfo[id].matched
				&& !jQuery.isEmptyObject(dbQuery.empFullInfo[id].matched[keywords[i]])) {
				var count = 0;
				for (var item in dbQuery.empFullInfo[id].matched[keywords[i]]) {
					if (dbQuery.matchedItems[keywords[i]][item])
						count++;
				}
				if (count != 0)
					continue;
				else {
					show = false;
					break;
				}
			}
			else {
				show = false;
				break;
			}
		}
		if (show) {
			var employee = $('<div class="employee"></div>');
			employee.append("<p><img height=200 class='img-rounded' src='img/" + emp.id + ".jpg' ></p>");
			var infoTable = $("<table  align='center' class='borderless table-hide'></table");
			var tr = $('<tr></tr>');
			//tr.append('<th>NO.</th>');
			tr.append('<td><a href="emp.html?id=' + emp.id + '">' + emp.number + '</a></td>');
			tr.appendTo(infoTable);

			tr = $('<tr></tr>');
			//tr.append('<th>Name</th>');
			tr.append('<td>' + emp.name + '</td>');
			tr.appendTo(infoTable);

			tr = $('<tr></tr>');
			//tr.append('<th>DOB.</th>');
			tr.append('<td>' + emp.birthday + '</td>');
			tr.appendTo(infoTable);

			infoTable.appendTo(employee);
			employee.appendTo(largeView);
		}
	}
//
//	if (!q1) {
//		for (var i = 0; i < emps.length; i++) {
//			var emp = emps[i];
//
//			var employee = $('<div class="employee"></div>');
//			employee.append('<a href="emp.html?id=' + emp.id + '"><p><img height=200 class="img-rounded" src="img/' + emp.id + '.jpg" ></p>');
//			var infoTable = $("<table align='center' class='borderless table-hide'></table");
//			var tr = $('<tr></tr>');
//			//tr.append('<th>NO.</th>');
//			tr.append('<td><a href="emp.html?id=' + emp.id + '">' + emp.number + '</a></td>');
//			tr.appendTo(infoTable);
//
//			tr = $('<tr></tr>');
//			//tr.append('<th>Name</th>');
//			tr.append('<td>' + emp.name + '</td>');
//			tr.appendTo(infoTable);
//
//			tr = $('<tr></tr>');
//			//tr.append('<th>DOB.</th>');
//			tr.append('<td>' + emp.birthday + '</td>');
//			tr.appendTo(infoTable);
//
//			infoTable.appendTo(employee);
//			employee.appendTo(largeView);
//		}
//	}
	empViews.loadLocalImage();
};
empViews.showGeneralView = function() {
	if (!q1 && jQuery.isEmptyObject(dbQuery.empFullInfo)) dbQuery.getAllEmps();
	var generalView = $('div#general-view');
	generalView.find('table').remove();
	$('div#detailed-view').find('table').remove();
	$('div#large-view').find('div.employee').remove();

	var table = $('<table class="table table-striped"><thead></thead><tbody id="general-main-tbody"></tbody></table');
	var tr = $('<tr id="result-header"></tr>');
	tr.append("<th></th>");
	tr.append("<th class='drag-enable'>Number</th>");
	tr.append("<th class='drag-enable'>Name</th>");
	tr.append("<th class='drag-enable'>Gender</th>");
	tr.append("<th class='drag-enable'>Date of Birth</th>");
	tr.append("<th class='drag-enable'>Telephone</th>");
	tr.appendTo(table.find('thead'));

	for (var id in dbQuery.empFullInfo){
		var emp = dbQuery.empFullInfo[id].emp;
		var show = true;
		for (var i = 0; i < keywords.length; i++) {
			if (keywords[i] in dbQuery.empFullInfo[id].matched
				&& !jQuery.isEmptyObject(dbQuery.empFullInfo[id].matched[keywords[i]])) {
				var count = 0;
				for (var item in dbQuery.empFullInfo[id].matched[keywords[i]]) {
					if (dbQuery.matchedItems[keywords[i]][item])
						count++;
				}
				if (count != 0)
					continue;
				else {
					show = false;
					break;
				}
			}
			else {
				show = false;
				break;
			}
		}
		if (show) {
			var tr = $('<tr></tr>');
			tr.append('<td><img height=40 class="img-circle" src="img/' + emp.id + '.jpg"></td>');
			tr.append('<td><a href="emp.html?id=' + emp.id + '">' + emp.number + '</a></td>');
			tr.append('<td>' + emp.name + '</td>');
			tr.append('<td>' + DB.choice(emp.sex) + '</td>');
			tr.append('<td>' + emp.birthday + '</td>');
			tr.append('<td>' + emp.tel + '</td>');
			if (q1)
				generateMatchedItemsTable(id, dbQuery.matchedItems).appendTo(tr);
			tr.appendTo(table.find('tbody#general-main-tbody'));
		}
	}

//	if (!q1) {
//		for (var i = 0; i < emps.length; i++) {
//			var emp = emps[i];
//			//console.log(emp);
//			var tr = $('<tr></tr>');
//			tr.append('<td><img height=40 class="img-circle" src="img/' + emp.id + '.jpg"></td>');
//			tr.append('<td><a href="emp.html?id=' + emp.id + '">' + emp.number + '</a></td>');
//			tr.append('<td>' + emp.name + '</td>');
//			tr.append('<td>' + DB.choice(emp.sex) + '</td>');
//			tr.append('<td>' + emp.birthday + '</td>');
//			tr.append('<td>' + emp.tel + '</td>');
//			tr.appendTo(table.find('tbody#general-main-tbody'));
//		}
//	}
//	else 
	if (q1){
		//Added Extra matched column
		table.find('tr#result-header').append('<th id="only-for-search">keyword/matched item/value</th>');
	}
	table.appendTo(generalView);
	empViews.loadLocalImage();
};
empViews.showDetailedView = function() {
	if (!q1 && jQuery.isEmptyObject(dbQuery.empFullInfo)) dbQuery.getAllEmps();

	var detailedView = $('div#detailed-view');
	$('div#general-view').find('table').remove();
	detailedView.find('table').remove();
	$('div#large-view').find('div.employee').remove();

	var table = $('<table class="table table-hover table-bordered table-condensed"><thead></thead><tbody></tbody></table>');
	//thead
	var tr = $('<tr></tr>');
	var tr2 = $('<tr></tr>')
	dbQuery.empCols.forEach(function(col) {
		tr.append('<th class="drag-enable" rowspan="2">' + dbQuery.colName[col] + '</th>');
	});
	//family
	tr.append('<th class="success" colspan="' + dbQuery.familyCols.length + '">' + 'Family' + '</th>');
	dbQuery.familyCols.forEach(function(col) {
		tr2.append('<th class="success">' + dbQuery.colName[col] + '</th>');
	});
	//address
	tr.append('<th class="info" colspan="' + dbQuery.addrCols.length + '">' + 'Adreess' + '</th>');
	dbQuery.addrCols.forEach(function(col) {
		tr2.append('<th class="info">' + dbQuery.colName[col] + '</th>');
	});
	//edu
	tr.append('<th class="danger" colspan="' + dbQuery.eduCols.length + '">' + 'School' + '</th>');
	dbQuery.eduCols.forEach(function(col) {
		tr2.append('<th class="danger">' + dbQuery.colName[col] + '</th>');
	});

	tr.appendTo(table.find('thead'));
	tr2.appendTo(table.find('thead'));

	//data
	for (var id in dbQuery.empFullInfo){
		var emp = dbQuery.empFullInfo[id].emp;
		var edu = dbQuery.empFullInfo[id].edu;
		var addr = dbQuery.empFullInfo[id].addr;
		var family = dbQuery.empFullInfo[id].family;
		var numOfNestedRows = Math.max(Math.max(family.length, addr.length), edu.length);
		numOfNestedRows = Math.max(numOfNestedRows, 1);
		var show = true;
		for (var i = 0; i < keywords.length; i++) {
			if (keywords[i] in dbQuery.empFullInfo[id].matched
				&& !jQuery.isEmptyObject(dbQuery.empFullInfo[id].matched[keywords[i]])) {
				var count = 0;
				for (var item in dbQuery.empFullInfo[id].matched[keywords[i]]) {
					if (dbQuery.matchedItems[keywords[i]][item])
						count++;
				}
				if (count != 0)
					continue;
				else {
					show = false;
					break;
				}
			}
			else {
				show = false;
				break;
			}
		}
		if (show) {
			//console.log(emp);
			var tr = $('<tr></tr>');
			//click the whole row;
			tr.click( function() {
				window.location = $(this).find('a').attr('href');
			});
			//tr.append('<td><img height=40 class="img-circle" src="img/' + emp.id + '.jpg"></td>');
			dbQuery.empColsPre.forEach(function(col) {
				var colWithoutPrefix = col.split(".")[1];
				if (colWithoutPrefix == 'number')
					tr.append('<td rowspan="' + numOfNestedRows + '"><a href="emp.html?id=' + emp.id + '">' + empViews.wrapTdValue(id, col, emp[colWithoutPrefix]) + '</a></td>');
				else if ($.inArray(col, dbQuery.itemsNeedToBeConverted) != -1) {
					tr.append('<td rowspan="' + numOfNestedRows + '">' + empViews.wrapTdValue(id, col, DB.choice(emp[colWithoutPrefix])) + '</td>');
				}
				else
					tr.append('<td rowspan="' + numOfNestedRows + '">' + empViews.wrapTdValue(id, col, emp[colWithoutPrefix]) + '</td>');
			});

			//helper function to fill family, address, edu
			var helperToFillFAE = function(i, list, cols, newTr, colorType) {
				if (typeof list[i] !== 'undefined') {
					cols.forEach(function(col) {
						var colWithoutPrefix = col.split('.')[1];
						if ($.inArray(col, dbQuery.itemsNeedToBeConverted) != -1) {
							newTr.append('<td class="' + colorType + '">' + empViews.wrapTdValue(id, col, DB.choice(list[i][colWithoutPrefix])) + '</td>');
						}
						else
							newTr.append('<td class="' + colorType + '">' + empViews.wrapTdValue(id, col, list[i][colWithoutPrefix]) + '</td>');
					});
				}
				else {
					newTr.append('<td class="' + colorType + '" colspan="' + cols.length + '"></td>');
				}
				return newTr;
			};

			for(var i = 0; i < numOfNestedRows; i++) {
				tr = helperToFillFAE(i, family, dbQuery.familyCols, tr, "success");
				tr = helperToFillFAE(i, addr, dbQuery.addrCols, tr, "info");
				tr = helperToFillFAE(i, edu, dbQuery.eduCols, tr, "danger");

				tr.appendTo(table.find('tbody'));
				tr = $('<tr class="table-sorter-child-row"></tr>');
			}

		}
	}
	table.appendTo(detailedView);
	empViews.loadLocalImage();
	//zoom to fit
	var zoomRate =  table.parent().width()/table.width();
	table.css('zoom', zoomRate);
};
empViews.showLargeViewPaging = function() {
	if (!q1 && jQuery.isEmptyObject(dbQuery.empFullInfo)) dbQuery.getAllEmps();
	var largeView = $('div#large-view');
	$('div#general-view').find('table').remove();
	$('div#detailed-view').find('table').remove();
	largeView.find('div.employee').remove();

	for (var id in dbQuery.empFullInfo){
		var emp = dbQuery.empFullInfo[id].emp;
		var show = true;
		for (var i = 0; i < keywords.length; i++) {
			if (keywords[i] in dbQuery.empFullInfo[id].matched
				&& !jQuery.isEmptyObject(dbQuery.empFullInfo[id].matched[keywords[i]])) {
				var count = 0;
				for (var item in dbQuery.empFullInfo[id].matched[keywords[i]]) {
					if (dbQuery.matchedItems[keywords[i]][item])
						count++;
				}
				if (count != 0)
					continue;
				else {
					show = false;
					break;
				}
			}
			else {
				show = false;
				break;
			}
		}
		if (show) {
			var employee = $('<div class="employee"></div>');
			employee.append("<p><img height=200 class='img-rounded' src='img/" + emp.id + ".jpg' ></p>");
			var infoTable = $("<table  align='center' class='borderless table-hide'></table");
			var tr = $('<tr></tr>');
			//tr.append('<th>NO.</th>');
			tr.append('<td><a href="emp.html?id=' + emp.id + '">' + emp.number + '</a></td>');
			tr.appendTo(infoTable);

			tr = $('<tr></tr>');
			//tr.append('<th>Name</th>');
			tr.append('<td>' + emp.name + '</td>');
			tr.appendTo(infoTable);

			tr = $('<tr></tr>');
			//tr.append('<th>DOB.</th>');
			tr.append('<td>' + emp.birthday + '</td>');
			tr.appendTo(infoTable);

			infoTable.appendTo(employee);
			employee.appendTo(largeView);
		}
	}

	if (!q1) {
		for (var i = 0; i < emps.length; i++) {
			var emp = emps[i];

			var employee = $('<div class="employee"></div>');
			employee.append('<a href="emp.html?id=' + emp.id + '"><p><img height=200 class="img-rounded" src="img/' + emp.id + '.jpg" ></p>');
			var infoTable = $("<table align='center' class='borderless table-hide'></table");
			var tr = $('<tr></tr>');
			//tr.append('<th>NO.</th>');
			tr.append('<td><a href="emp.html?id=' + emp.id + '">' + emp.number + '</a></td>');
			tr.appendTo(infoTable);

			tr = $('<tr></tr>');
			//tr.append('<th>Name</th>');
			tr.append('<td>' + emp.name + '</td>');
			tr.appendTo(infoTable);

			tr = $('<tr></tr>');
			//tr.append('<th>DOB.</th>');
			tr.append('<td>' + emp.birthday + '</td>');
			tr.appendTo(infoTable);

			infoTable.appendTo(employee);
			employee.appendTo(largeView);
		}
	}
	empViews.loadLocalImage();
};
empViews.wrapTdValue = function (id, item, value) {
	if (!q1) return value;//no keyword
	var matched = dbQuery.empFullInfo[id].matched;
	var count = 0;
	var matchedKw = [];
	var dbValueEqual = function (itemlist) {
		if ($.inArray(item, dbQuery.itemsNeedToBeConverted) != -1) {
			return DB.choice(itemlist[item]) == value;
		}
		else {
			return itemlist[item] == value;
		}
	}
	for (var kw in matched) {
		items = matched[kw];
		if (item in items && dbQuery.matchedItems[kw][item]) {//item matched and not excluded
			if (dbValueEqual(items)){
				count++;
				matchedKw.push(kw);
			}
		}
	}
	if (count == 0) return value;
	else			return empViews.highlightKeyWord([matchedKw, value]);
};
empViews.highlightKeyWord = function (args) {
	var ptnStr = args[0].map(function(ele) {
		return escapeHighlightKeyword(ele);
	}).join("|");
	//console.log(ptnStr);
	var ptn = new RegExp("("+ptnStr+")", ["gi"]);
	var newStr = "<span id='td-highlight-keyword'>$1</span>";
	//console.log(args[1].replace(ptn, newStr));
	return args[1].replace(ptn, newStr);
};
//----------------------empViews-----------------------------