var table;
var rows = [];
var dbQuery;
var MaxSelfDefinedColumns = 5;
var lastNewlyAddedColumn = MaxSelfDefinedColumns - 1;
(function() {
	dbQuery = JSON.parse(localStorage.getItem("dbQuery")) || {};
	//console.log(dbQuery);
	var existingTable = localStorage.getItem("ExisitingTable")||{};
	if (!jQuery.isEmptyObject(existingTable)){
		//Generate Table
		generateColumnSelecter();
		generateDetailedTable();
		//generateRows();
//		$('div#existing-table').find('a').addClass('toggle').attr('href', '#')
	}
	var zoomRate =  $('table').parent().width()/$('table').width();

	$("#slider").slider({
		slide: function(event, ui){
			var rate = $( "#slider" ).slider( "option", "value" )/100;
			$('table').css('zoom', rate);
		},
		value : zoomRate * 100
	});
	$('table').css('zoom', zoomRate);
	//Table sorter
	$(document).ready(function() {
		reinitialize();
	});
})();
var selfDefinedColumns = [];
for (var i = 0; i < MaxSelfDefinedColumns; i++) {
	selfDefinedColumns.push(i);
};

var Options = {};
var elementIds = {};
elementIds.emp = ['emp-img'];
dbQuery.empColsPre.forEach(function(ele) {
	elementIds.emp.push(ele.replace(/\./g, "-"));
});
elementIds.family = [];
dbQuery.familyCols.forEach(function(ele) {
	elementIds.family.push(ele.replace(/\./g, "-"));
});
elementIds.addr = [];
dbQuery.addrCols.forEach(function(ele) {
	elementIds.addr.push(ele.replace(/\./g, "-"));
});
elementIds.edu = [];
dbQuery.eduCols.forEach(function(ele) {
	elementIds.edu.push(ele.replace(/\./g, "-"));
});

//initialize
Options.checkedlist = {emp:true, family:true, addr:true, edu:true};
Options.emp = elementIds.emp.slice();
Options.family = elementIds.family.slice();
Options.addr = elementIds.addr.slice();
Options.edu = elementIds.edu.slice();
$('.dropdown-menu a').find('input').prop('checked', true);

$( '.dropdown-menu a:not(.columns-group)' ).on( 'click', function( event ) {
	var $target = $( event.currentTarget ),
		val = $target.attr( 'data-value' ),
		group = val.split('-')[0],
		$inp = $target.find( 'input' ),
		idx;

	var numOfSpan = parseInt($('th#'+group+'-header').attr('colspan'));

	if ( ( idx = Options[group].indexOf( val ) ) > -1 ) {
		Options[group].splice( idx, 1 );
		if (numOfSpan == 1){
			//remove select all
			$('th#'+group+'-header').hide();
		}
		$('th#'+group+'-header').attr('colspan', numOfSpan-1);
		$('th#'+val).hide();
		$('td#'+val).hide();
		setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
	} else {
		Options[group].push( val );
		if (numOfSpan == 0){
			$('th#'+group+'-header').show();
		}
		$('th#'+group+'-header').attr('colspan', numOfSpan+1);
		$('th#'+val).show();
		$('td#'+val).show();
		setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
	}

	$( event.target ).blur();
	$('table').trigger("updateAll");
	//reinitialize();
	//console.log( Options );
	return false;
});
$('.dropdown-menu a.columns-group').on('click', function(event){
	var $target = $( event.currentTarget),
		val = $target.attr('data-value'),
		$inp = $target.find('input');
	//change state
	Options.checkedlist[val] = !Options.checkedlist[val];


	if (Options.checkedlist[val]) {

		Options[val] = elementIds[val].slice();
		$('th#'+val+'-header').attr('colspan', elementIds[val].length);
		$('th#'+val+'-header').show();

		Options[val].forEach(function(ele) {
			$('th#'+ele).show();
			$('td#'+ele).show();
		});
		$('a[data-value*="'+val+'-"').find('input').prop('checked', true);
		setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
	}
	else {
		$('th#'+val+'-header').hide();
		$('th#'+val+'-header').attr('colspan', 0);
		Options[val].forEach(function(ele) {
			$('th#'+ele).hide();
			$('td#'+ele).hide();
		});
		Options[val] = [];
		$('a[data-value*="'+val+'-"').find('input').prop('checked', false);
		setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
	}

	$( event.target).blur();
	$('table').trigger("updateAll");
	//reinitialize();
	//console.log( Options );
	return false;
});

$('a#new-column').on('click', function(event) {
	if (selfDefinedColumns.length == 0) alert("You can only define " + MaxSelfDefinedColumns + " columns!");
	else {
		$('th#self-header').show();
		var columnIdx = selfDefinedColumns[0];
		var newColumnName = $('input#column-name').val();

		var li = $('<li class="small"><a href="#">' + newColumnName + '</a></li>');
		var button = $('<button  id="self-' + columnIdx + '" type="button" class="close" aria-label="Close" data-toggle="tooltip" data-placement="top" title="Delete This Column"><span aria-hidden="true">×</span></button>');
		button.click(function(){
			var colId = $(this).attr('id');
			deleteColumn(colId);
		});
		button.appendTo(li.find('a'));
		li.appendTo($('ul#added-columns'));


		selfDefinedColumns.splice(0, 1);
		$('th#self-'+columnIdx).show().text(newColumnName).insertAfter($('th#self-'+lastNewlyAddedColumn));
		$('th#self-')
		$('td#self-'+columnIdx).show();
		$('td#self-'+columnIdx).each(function() {
			$(this).insertAfter($(this).siblings('td#self-'+lastNewlyAddedColumn));
		});
		lastNewlyAddedColumn = columnIdx;
		$('th#self-header').attr("colspan", parseInt($('th#self-header').attr("colspan")) + 1);
		$('table').trigger("updateAll");
		//reinitialize();
		//console.log(selfDefinedColumns);
	}
});

// $('td[id*="self-"]').on('click', function(event) {
// 	var $target = $( event.currentTarget),
// 		text = $target.text();
// 	$target.empty();
// 	$target.append("<input value='" + text + "'/>");
// });
$('td[id*="self-"]').on('blur', function() {
	$('table').trigger("update");
	//reinitialize();
});
function reinitialize() {
	$("table").tablesorter({
		  theme : 'blue',
		  emptyTo : "bottom",
		  cssChildRow: "table-sorter-child-row",
		  dateFormat: "yyyymmdd",
		});//.tablesorterPager({container: $("div#pager")});

}
function deleteColumn(colId) {
	selfDefinedColumns.push(parseInt(colId.split('-')[1]));
	$('button#'+colId).closest('li').remove();
	$('th#'+colId).hide().text("");
	$('td#'+colId).hide().text("");
	$('th#self-header').attr("colspan", parseInt($('th#self-header').attr("colspan")) - 1);
	if (parseInt($('th#self-header').attr("colspan")) == 0) $('th#self-header').hide();
	$('table').trigger("updateAll");

	//reinitialize();
};
function generateColumnSelecter(){
	var ul = $('ul#columns');
	//emp;
	ul.append('<li><a href="#" class="small columns-group" data-value="emp" tabIndex="-1">\
			<input type="checkbox"/><strong>&nbsp;All Employees Columns</strong></a></li>');
	ul.append('<li><a href="#" class="small" data-value="emp-img" tabIndex="-1"><input type="checkbox"/>&nbsp;Photo</a></li>');
	dbQuery.empCols.forEach(function(col) {
		ul.append('<li><a href="#" class="small" data-value="emp-'+ col + '" tabIndex="-1"><input type="checkbox"/>&nbsp;' + dbQuery.colName[col] + '</a></li>');
	});
	ul.append('<li role="separator" class="divider"></li>');
	//Family
	ul.append('<li><a href="#" class="small columns-group" data-value="family" tabIndex="-1"><input type="checkbox"/><strong>&nbsp;Family</strong></a></li>');
	dbQuery.familyCols.forEach(function(col) {
		ul.append('<li><a href="#" class="small" data-value="'+ col.replace(/\./g, "-") + '" tabIndex="-1"><input type="checkbox"/>&nbsp;' + dbQuery.colName[col] + '</a></li>');
	});
	ul.append('<li role="separator" class="divider"></li>');
	//Address
	ul.append('<li><a href="#" class="small columns-group" data-value="addr" tabIndex="-1"><input type="checkbox"/><strong>&nbsp;Address</strong></a></li>');
	dbQuery.addrCols.forEach(function(col) {
		ul.append('<li><a href="#" class="small" data-value="'+ col.replace(/\./g, "-") + '" tabIndex="-1"><input type="checkbox"/>&nbsp;' + dbQuery.colName[col] + '</a></li>');
	});
	ul.append('<li role="separator" class="divider"></li>');
	//Edu
	ul.append('<li><a href="#" class="small columns-group" data-value="edu" tabIndex="-1"><input type="checkbox"/><strong>&nbsp;Schools</strong></a></li>');
	dbQuery.eduCols.forEach(function(col) {
		ul.append('<li><a href="#" class="small" data-value="'+ col.replace(/\./g, "-") + '" tabIndex="-1"><input type="checkbox"/>&nbsp;' + dbQuery.colName[col] + '</a></li>');
	});
	ul.append('<li role="separator" class="divider"></li>');
};
function generateDetailedTable() {
	var detailedView = $('div#existing-table');

	var table = $('<table style="overflow-x: scroll;" class="table"><thead></thead><tbody></tbody></table>');
	//thead
	var tr = $('<tr></tr>');
	var tr2 = $('<tr></tr>');
	//emp
	tr.append('<th id="emp-header" colspan="' + (dbQuery.empCols.length + 1) + '" data-sorter="false">' + 'Employee Personal Info' + '</th>');
	tr2.append('<th id="emp-img" data-sorter="false"></th>');
	dbQuery.empCols.forEach(function(col) {
		tr2.append('<th id="emp-' + col + '">' + dbQuery.colName[col] + '</th>');
	});

	//hide customized columns
	tr.append('<th id="self-header"  class="warning" colspan="0" style="display:none;" data-sorter="false">Self Defined Columns</th>');
	for(var i = 0; i < MaxSelfDefinedColumns; i++) {
		tr2.append('<th id="self-' + i + '" class="warning" style="display:none;"></th>');
	}
	//family
	tr.append('<th id="family-header" class="success" colspan="' + dbQuery.familyCols.length + '" data-sorter="false">' + 'Family' + '</th>');
	dbQuery.familyCols.forEach(function(col) {
		tr2.append('<th id="' + col.replace(/\./g, "-") + '" class="success">' + dbQuery.colName[col] + '</th>');
	});
	//address
	tr.append('<th id="addr-header" class="info" colspan="' + dbQuery.addrCols.length + '" data-sorter="false">' + 'Adreess' + '</th>');
	dbQuery.addrCols.forEach(function(col) {
		tr2.append('<th id="' + col.replace(/\./g, "-") + '" class="info">' + dbQuery.colName[col] + '</th>');
	});
	//edu
	tr.append('<th id="edu-header" class="danger" colspan="' + dbQuery.eduCols.length + '" data-sorter="false">' + 'School' + '</th>');
	dbQuery.eduCols.forEach(function(col) {
		tr2.append('<th id="' + col.replace(/\./g, "-") + '" class="danger">' + dbQuery.colName[col] + '</th>');
	});

	tr.appendTo(table.find('thead'));
	tr2.appendTo(table.find('thead'));

	//data
	for (var id in dbQuery.empFullInfo){
		var emp = dbQuery.empFullInfo[id].emp;
		var edu = dbQuery.empFullInfo[id].edu;
		var addr = dbQuery.empFullInfo[id].addr;
		var family = dbQuery.empFullInfo[id].family;
		var numOfNestedRows = Math.max(Math.max(family.length, addr.length), edu.length);
		numOfNestedRows = Math.max(numOfNestedRows, 1);
		var show = true;
		for (var keyword in dbQuery.matchedItems){
			if (keyword in dbQuery.empFullInfo[id].matched
				&& !jQuery.isEmptyObject(dbQuery.empFullInfo[id].matched[keyword])) {
				var count = 0;
				for (var item in dbQuery.empFullInfo[id].matched[keyword]) {
					if (dbQuery.matchedItems[keyword][item])
						count++;
				}
				if (count != 0)
					continue;
				else {
					show = false;
					break;
				}
			}
			else {
				show = false;
				break;
			}
		}
		if (show) {
			//console.log(emp);
			var tr = $('<tr id="' + emp.id +'"></tr>');

			tr.append('<td id="emp-img" rowspan="' + numOfNestedRows + '"><img height=40 class="img-circle" src="img/' + emp.id + '.jpg"></td>');
			dbQuery.empColsPre.forEach(function(col) {
				var colWithoutPrefix = col.split(".")[1];
				if (colWithoutPrefix == 'number')
					tr.append('<td id="' + col.replace(/\./g, "-") + '" rowspan="' + numOfNestedRows + '">' + emp[colWithoutPrefix] + '</td>');
				else if ($.inArray(col, dbQuery.itemsNeedToBeConverted) != -1) {
					tr.append('<td id="' + col.replace(/\./g, "-") + '" rowspan="' + numOfNestedRows + '">' + DB.choice(emp[colWithoutPrefix]) + '</td>');
				}
				else
					tr.append('<td id="' + col.replace(/\./g, "-") + '" rowspan="' + numOfNestedRows + '">' + emp[colWithoutPrefix] + '</td>');
			});

			//hide columns
			for(var i = 0; i < MaxSelfDefinedColumns; i++) {
				tr.append('<td id="self-' + i + '" class="warning" contenteditable="true" rowspan="' + numOfNestedRows + '" style="display:none;"></td>');
			}

			//helper function to fill family, address, edu
			var helperToFillFAE = function(i, list, cols, newTr, colorType) {
				if (typeof list[i] !== 'undefined') {
					cols.forEach(function(col) {
						var colWithoutPrefix = col.split('.')[1];
						if ($.inArray(col, dbQuery.itemsNeedToBeConverted) != -1) {
							newTr.append('<td id="' + col.replace(/\./g, "-") + '" class="' + colorType + '">' + DB.choice(list[i][colWithoutPrefix]) + '</td>');
						}
						else
							newTr.append('<td id="' + col.replace(/\./g, "-") + '" class="' + colorType + '">' + list[i][colWithoutPrefix] + '</td>');
					});
				}
				else {
					cols.forEach(function(col) {
						newTr.append('<td id="' + col.replace(/\./g, "-") + '" class="' + colorType + '"></td>');
					});
				}
				return newTr;
			};

			for(var i = 0; i < numOfNestedRows; i++) {
				tr = helperToFillFAE(i, family, dbQuery.familyCols, tr, "success");
				tr = helperToFillFAE(i, addr, dbQuery.addrCols, tr, "info");
				tr = helperToFillFAE(i, edu, dbQuery.eduCols, tr, "danger");

				tr.appendTo(table.find('tbody'));
				tr = $('<tr class="table-sorter-child-row" id="' + emp.id + '"></tr>');
			}

		}
	}
	table.appendTo(detailedView);
	loadLocalImage();
};

function generateRows() {
	//data
	for (var id in dbQuery.empFullInfo){
		var emp = dbQuery.empFullInfo[id].emp;
		var edu = dbQuery.empFullInfo[id].edu;
		var addr = dbQuery.empFullInfo[id].addr;
		var family = dbQuery.empFullInfo[id].family;
		var numOfNestedRows = Math.max(Math.max(family.length, addr.length), edu.length);
		numOfNestedRows = Math.max(numOfNestedRows, 1);
		var show = true;
//		for (var keyword in dbQuery.matchedItems){
//			if (keyword in dbQuery.empFullInfo[id].matched
//				&& !jQuery.isEmptyObject(dbQuery.empFullInfo[id].matched[keyword])) {
//				var count = 0;
//				for (var item in dbQuery.empFullInfo[id].matched[keyword]) {
//					if (dbQuery.matchedItems[keyword][item])
//						count++;
//				}
//				if (count != 0)
//					continue;
//				else {
//					show = false;
//					break;
//				}
//			}
//			else {
//				show = false;
//				break;
//			}
//		}
		if (show) {
			//console.log(emp);
			//console.log(id);
			var row = {};
			row.parent = {};
			row.children = [];
			var tr = $('<tr id="' + emp.id +'"></tr>');

			tr.append('<td id="emp-img" rowspan="' + numOfNestedRows + '"><img height=40 class="img-circle" src="img/' + emp.id + '.jpg"></td>');
			dbQuery.empColsPre.forEach(function(col) {
				var colWithoutPrefix = col.split(".")[1];
				if (colWithoutPrefix == 'number')
					tr.append('<td id="' + col.replace(/\./g, "-") + '" rowspan="' + numOfNestedRows + '">' + emp[colWithoutPrefix] + '</td>');
				else if ($.inArray(col, dbQuery.itemsNeedToBeConverted) != -1) {
					tr.append('<td id="' + col.replace(/\./g, "-") + '" rowspan="' + numOfNestedRows + '">' + DB.choice(emp[colWithoutPrefix]) + '</td>');
				}
				else
					tr.append('<td id="' + col.replace(/\./g, "-") + '" rowspan="' + numOfNestedRows + '">' + emp[colWithoutPrefix] + '</td>');
			});

			//hide columns
			for(var i = 0; i < MaxSelfDefinedColumns; i++) {
				tr.append('<td id="self-' + i + '" class="warning" contenteditable="true" rowspan="' + numOfNestedRows + '" style="display:none;"></td>');
			}

			//helper function to fill family, address, edu
			var helperToFillFAE = function(i, list, cols, newTr, colorType) {
				if (typeof list[i] !== 'undefined') {
					cols.forEach(function(col) {
						var colWithoutPrefix = col.split('.')[1];
						if ($.inArray(col, dbQuery.itemsNeedToBeConverted) != -1) {
							newTr.append('<td id="' + col.replace(/\./g, "-") + '" class="' + colorType + '">' + DB.choice(list[i][colWithoutPrefix]) + '</td>');
						}
						else
							newTr.append('<td id="' + col.replace(/\./g, "-") + '" class="' + colorType + '">' + list[i][colWithoutPrefix] + '</td>');
					});
				}
				else {
					cols.forEach(function(col) {
						newTr.append('<td id="' + col.replace(/\./g, "-") + '" class="' + colorType + '"></td>');
					});
				}
				return newTr;
			};

			for(var i = 0; i < numOfNestedRows; i++) {
				tr = helperToFillFAE(i, family, dbQuery.familyCols, tr, "success");
				tr = helperToFillFAE(i, addr, dbQuery.addrCols, tr, "info");
				tr = helperToFillFAE(i, edu, dbQuery.eduCols, tr, "danger");
				if (i == 0) row.parent = tr;
				else row.children.push(tr);
				tr = $('<tr class="table-sorter-child-row" id="' + emp.id + '"></tr>');
			}
			rows.push(row);
		}
	}
	console.log(rows);
};


function loadLocalImage() {
	var storageFiles = JSON.parse(localStorage.getItem("storageFiles")) || {};

	var elements = document.getElementsByTagName("img");

	if (jQuery.isEmptyObject(storageFiles)) {
		storageFiles.images = {};
	}
	//console.log(storageFiles.images);

	for (var i = 0; i < elements.length; i++) {
		var ele = elements[i];
		var imgId = ele.getAttribute('src');
		if (!(imgId in storageFiles.images)) {
			ele.setAttribute("src", imgId);
		}
		else {
			ele.setAttribute("src", storageFiles.images[imgId]);
		}
	}
};