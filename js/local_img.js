showLocalImg();
//Show images stored locally
function showLocalImg() {
	var storageFiles = JSON.parse(localStorage.getItem("storageFiles")) || {};

	var ele = document.getElementById("img-emp");

	var imgId = ele.getAttribute("src");

	if (!jQuery.isEmptyObject(storageFiles)) {
		if (imgId in storageFiles.images)
			ele.setAttribute("src", storageFiles.images[imgId]);
	}
};

var changeAllImageToLocal = function() {

	var storageFiles = JSON.parse(localStorage.getItem("storageFiles")) || {};

	var elements = document.getElementsByTagName("img");
	console.log(storageFiles);
	if (jQuery.isEmptyObject(storageFiles)) {
		storageFiles.images = {};
	}
	console.log(storageFiles.images);

	for (var i = 0; i < elements.length; i++) {
		var ele = elements[i];
		var imgId = ele.getAttribute('src');
		console.log(imgId);
		if (!(imgId in storageFiles.images)) {
			console.log(imgId + " is not in local storage, loading...");

			ele.addEventListener("load", function () {
				var id = $(this).attr('src');

				var img = new Image();
				img.src = id;

				var imgCanvas = document.createElement("canvas");
				imgCanvas.width = '29.83';
				imgCanvas.height = '40';

				var imgContext = imgCanvas.getContext("2d");

				// Make sure canvas is as big as the picture

				// Draw image into canvas element
				imgContext.drawImage(img, 0, 0, 29.83, 40);
				console.log(imgCanvas.toDataURL("image/png"));
				// Save image as a data URL
				storageFiles.images[id] = imgCanvas.toDataURL("image/jpeg", 1);

				// Save as JSON in localStorage
				try {
					localStorage.setItem("storageFiles", JSON.stringify(storageFiles));
					console.log("Storage success: " + id + ", elements length " + elements.length);
				}
				catch (e) {
					console.log("Storage failed: " + e);
				}
			}, false);

			ele.setAttribute("src", imgId);
		}
		else
		{
			ele.setAttribute("src", storageFiles.images[imgId]);
		}
	}
};
